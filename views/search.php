<?php
require_once '../classes/Client.php';
$input;

if(isset($_POST) && !empty($_POST))
{
  $input = $_POST['search'];
  if(!empty($input) && !is_numeric($input))
  {
    $client = new Client;
    $contact = $client->searchContacts('', $input);
  }

}
session_start();
$message = (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) ? $_SESSION['msg'] : '';

?>

<?php require_once 'header.php';?>

<?php if(!empty($message)): ?>
    <div class="alert alert-success" role="alert">
      <?php echo $message;unset($_SESSION['msg']); ?>
    </div>
<?php endif; ?>

    <form method="post">
        <div class="form-group">
            <label for="search"></label>
            <input type="text" class="form-control" id="search" name="search"   value="<?php echo !empty($input) ? $input : '';  ?>">
        </div>
        <button type="submit" name="submit" class="btn btn-primary">Search</button>
    </form>
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone Number</th>
            <th scope="col">Address</th>
            <th scope="col">edit/delete</th>
        </tr>
        </thead>
        <tbody>
        <?php $id = 1; if(!empty($contact)): ?>
          <?php  foreach ($contact as $val): ?>
                <tr>
                    <th scope="row"><?php echo $id; ?></th>
                    <td><?php echo $val['name']; ?></td>
                    <td><?php echo $val['email']; ?></td>
                    <td><?php echo $val['phoneNumber']; ?></td>
                    <td><?php echo $val['address']; $id++ ?></td>
                    <td>
                        <a href="edit.php?id=<?php echo $val['id'];?>">edit</a> /
                        <a href="delete.php?id=<?php echo $val['id'];?>">delete</a>
                    </td>
                </tr>
          <?php endforeach; ?>
        <?php elseif($id==1): ?>
            <tr>
                <th scope="row">#</th>
                <td>Not Result</td>
            </tr>
        <?php endif; ?>

        </tbody>
    </table>

<?php require_once 'footer.php';?>