<?php
require_once '../classes/Client.php';


if((isset($_GET) && !empty($_GET))){
  $id=$_GET['id'];
  $client = new Client;
  $contact = $client->deleteContact($id);
  if($contact){
    session_start();
    $_SESSION['msg'] = 'successfully deleted.';
    header('location: list.php');
  }
}