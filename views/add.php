<?php
require_once '../classes/Client.php';

$contact = null;
$messages = [];
$post;

if(isset($_POST) && !empty($_POST))
{
    $client = new Client;
    $post=$_POST;
    $contact = $client->addContact($_POST);

    if($contact === '23000')
    {
        $messages[] = 'Phone number already exists';
    }
    elseif($contact== '1')
    {
      session_start();
      $_SESSION['msg'] = 'successfully added.';
      header('location: list.php');
    }
    else
    {
        $messages[]='no successfully added.';
    }
}


?>

<?php require_once 'header.php';?>
<?php foreach ($messages as $message): ?>
    <p> <?php  echo $message; ?></p>
<?php endforeach; ?>
  <form method="post">
    <div class="form-group">
      <label for="name">Name: </label>
      *<input type="text" class="form-control" id="name" name="name" required value="<?php echo (isset($post) && !empty($post)) ? $post['name'] : '';  ?>">
    </div>
    <div class="form-group">
      <label for="email">Email: </label>
      <input type="email" class="form-control" id="email" name="email" value="<?php echo (isset($post) && !empty($post)) ? $post['email'] : '';  ?>">
    </div>
    <div class="form-group">
      <label for="phoneNumber">Phone number: </label>
      *<input type="text" class="form-control" id="phoneNumber" name="phoneNumber" required value="<?php echo (isset($post) && !empty($post)) ? $post['phoneNumber'] : '';  ?>">
    </div>
    <div class="form-group">
      <label for="address">Address: </label>
      <textarea name="address" name="address" id="address" cols="150" rows="5" ><?php echo (isset($post) && !empty($post)) ? $post['address'] : '';  ?></textarea>
    </div>
    <button type="submit" name="submit" class="btn btn-primary" value="add">ADD</button>
  </form>

<?php require_once 'footer.php';?>