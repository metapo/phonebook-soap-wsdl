<?php
    $currentPath = strtolower(basename($_SERVER['SCRIPT_FILENAME']));
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="style/bootstrap.css"/>
    <link rel="stylesheet" href="style/style.css"/>

</head>
<body>

<div class="container">
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link <?php  if($currentPath == 'list.php') echo 'active'?>"   id="nav-listContact-tab"  href="list.php"">List Contact</a>
            <a class="nav-item nav-link <?php  if($currentPath == 'add.php') echo 'active'?>" id="nav-addContact-tab"     href="add.php" " >Add Contact</a>
            <a class="nav-item nav-link <?php  if($currentPath == 'search.php') echo 'active'?>" id="nav-searchContact-tab"  href="search.php" >Search Contact</a>
            <?php if($currentPath == 'edit.php'): ?>
                <a class="nav-item nav-link active" id="nav-searchContact-tab"  href="edit.php" >Edit Contact</a>
            <?php endif;  ?>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
