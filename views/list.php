<?php
require_once '../classes/Client.php';
$client = new Client;
$contact = $client->listContacts();
session_start();
$message = (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) ? $_SESSION['msg'] : '';
?>

<?php require_once 'header.php';?>

    <?php if(!empty($message)): ?>
        <div class="alert alert-success" role="alert">
            <?php echo $message;unset($_SESSION['msg']); ?>
        </div>
    <?php endif; ?>
    <table class="table table-hover">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Email</th>
          <th scope="col">Phone Number</th>
          <th scope="col">Address</th>
          <th scope="col">edit/delete</th>
        </tr>
        </thead>
        <tbody>
            <?php if($contact): ?>
              <?php $id = 1; foreach ($contact as $val): ?>
                <tr>
                  <th scope="row"><?php echo $id; ?></th>
                  <td><?php echo $val['name']; ?></td>
                  <td><?php echo $val['email']; ?></td>
                  <td><?php echo $val['phoneNumber']; ?></td>
                  <td><?php echo $val['address']; $id++ ?></td>
                  <td>
                    <a href="edit.php?id=<?php echo $val['id'];?>">edit</a> /
                    <a href="delete.php?id=<?php echo $val['id'];?>">delete</a>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php endif; ?>

        </tbody>
    </table>

<?php require_once 'footer.php';?>