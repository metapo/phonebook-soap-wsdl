<?php
include_once 'db.php';
class model
{
  private $id;
  private $name;
  private $email;
  private $phoneNumber;
  private $address;
  private $conn;
  private $data ;

  public function __construct()
  {
    session_start();
    $this->conn = (is_null($this->conn)) ? db::connect() : $this->conn;
  }

  public function prepare($data = '')
  {
    if(array_key_exists('id' , $data))
    {
      $this->id = $data['id'];
    }

    if(array_key_exists('name' , $data))
    {
      $this->name = $data['name'];
    }

    if(array_key_exists('email' , $data))
    {
      $this->email = $data['email'];
    }
    if(array_key_exists('phoneNumber' , $data))
    {
      $this->phoneNumber = $data['phoneNumber'];
    }
    if(array_key_exists('address' , $data))
    {
      $this->address = $data['address'];
    }

    return $this;
  }


  public function show($field= ''){
    if(empty($field))
    {
      $sql = "SELECT * FROM contact ORDER BY name ASC";
    }
    else
    {
      if(is_numeric($field))
        $sql = "SELECT * FROM contact WHERE id = :field ORDER BY name ASC";
      else
        $sql = "SELECT * FROM contact WHERE name = :field ORDER BY name ASC";
    }
    $stmt = $this->conn->prepare($sql);
    if(!empty($field))
    {
      $stmt->bindParam(':field', $field);
    }
    $stmt->execute();
    while($row = $stmt->fetch(PDO::FETCH_ASSOC))
    {
      $this->data[] = $row;
    }
    return $this->data;
  }

  public function insert()
  {
    try{
      if( isset($this->name)   && !empty($this->name) &&
        isset($this->email) &&
        isset($this->phoneNumber) && !empty($this->phoneNumber) &&
        isset($this->address))
      {
        $sql = "INSERT  INTO    contact(name,email,phoneNumber,address)
                          VALUES  (:name , :email, :phoneNumber, :address)";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':phoneNumber', $this->phoneNumber);
        $stmt->bindParam(':address', $this->address);
        $stmt->execute();
        return true;

      }
    }
    catch(\Exception $e){
      return $e->getCode();
    }
  }

  public function update()
  {
    try{
      if( isset($this->name)   && !empty($this->name) &&
        isset($this->email) &&
        isset($this->phoneNumber) && !empty($this->phoneNumber) &&
        isset($this->address))
      {
        $sql = "UPDATE contact SET name = :name ,email = :email,phoneNumber = :phoneNumber,address = :address
                               WHERE id = :id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':phoneNumber', $this->phoneNumber);
        $stmt->bindParam(':address', $this->address);
        $stmt->bindParam(':id', $this->id);
        $stmt->execute();
        return true;
      }
    }
    catch(\Exception $e){
      return $e->getCode();
    }
  }

  public function delete($id)
  {
      $sql = "DELETE  FROM contact WHERE id= :id";
      $stmt = $this->conn->prepare($sql);
      $stmt->bindParam(':id', $id);
      $stmt->execute();
      return true;
  }
}



