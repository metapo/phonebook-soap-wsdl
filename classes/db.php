<?php
class db
{
  private static $servername = "localhost";
  private static $username = "root";
  private static $password = "";

  static function connect(){
    try {
      $conn = new PDO("mysql:host=".self::$servername, self::$username, self::$password);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sql = "CREATE DATABASE IF NOT EXISTS phonebook";
      $conn->exec($sql);
      $sql = "use phonebook";
      $conn->exec($sql);
      $sql = "CREATE TABLE IF NOT EXISTS contact(
                id int(11) AUTO_INCREMENT PRIMARY KEY,
                name varchar(30) NOT NULL ,
                email VARCHAR(50),
                phoneNumber VARCHAR(11) NOT NULL UNIQUE ,
                address VARCHAR(250) );";
      $conn->exec($sql);
      return $conn;
    }
    catch(PDOException $e)
    {
      echo $e->getMessage();
    }
  }
}