<?php
require_once 'Model.php';
$server = new SoapServer('../phonebook.wsdl');
class Server
{
  public function listContacts()
  {
    $contact = new Model();
    return  json_encode($contact->show());
  }

  public function addContact($data)
  {
    try{
      $contact = new Model();
      return json_encode($contact->prepare(json_decode($data,true))->insert());
    }
    catch ( SoapFault $e ) { // Do NOT try and catch "Exception" here
      return $e;
    }
  }

  public function searchContacts($field)
  {
    $contact = new Model();
    return  json_encode($contact->show(json_decode($field, true)));
  }

  public function deleteContact($id)
  {
      $contact = new Model();
      return json_encode($contact->delete(json_decode($id,true)));
  }
  public function editContact($data)
  {
    $contact = new Model;
    return json_encode($contact->prepare(json_decode($data,true))->update());
  }
}

$server->SetClass("Server");
$server->handle();

