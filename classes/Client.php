<?php

class Client
{
  private $client;
  public function __construct()
  {
    try{
      $this->client = new SoapClient("../phonebook.wsdl",[
        'cache_wsdl' => WSDL_CACHE_NONE,
        'trace' => 1
      ]);
    }
    catch ( SoapFault $e ) { // Do NOT try and catch "Exception" here
      echo 'sorry... our service is down';
    }
  }

  public function listContacts()
  {
    $result = json_decode($this->client->listContacts(), true);
    $this->log();
    return $result;
  }

  public function addContact($data)
  {
    $result = json_decode($this->client->addContact(json_encode($data)),true);
    $this->log();
    return $result;
  }

  public function searchContacts($id = '' , $name = '')
  {
    if(!empty($id))
      $field = $id;
    else
      $field = $name;
    $result = json_decode($this->client->searchContacts(json_encode($field)), true);
    $this->log();
    return $result;
  }

  public function editContact($data){
    $result = json_decode($this->client->editContact(json_encode($data)),true);
    $this->log();
    return $result;
  }

  public function deleteContact($id)
  {
    $result = json_decode($this->client->deleteContact(json_encode($id)),true);
    $this->log();
    return $result;
  }

  public function log(){
    $request = $this->client->__getLastRequest();//
    $responsesH = $this->client->__getLastResponseHeaders();
    $responseH  = explode('Server:',$responsesH)[0];//
    $response = $this->client->__getLastResponse();//
    $file = '../log/ReqRes.xml';
    $current = "Request:\n{$request}\nHeader Response:\n{$responseH}\nResponse:\n{$response}\n\n\n";
    file_put_contents($file, $current,FILE_APPEND);
  }
}

